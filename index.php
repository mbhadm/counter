<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <script src="jquery-1.11.3.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="style.css">
    </head>
    <body>
        
        <div class="container">
            <div class="row">
                <div class="timer">
                    
                    <div id="seconds">10 : 00</div>
                    <button onclick="startCount()">Start tiden</button>
                    <button onclick="pauseCount()">Pause</button>
                    <button onclick="resetCount()">Start forfra</button>
                </div>
            </div>
        </div>
    </body>
</html>

<script>
        // Our timer variables used to countdown
        var minutes = 9;
        var seconds = 60;
        var timerStatus = 0;

        // If we want to pause our timer
        var pause;

        // Our time counter function that will count down from given minutes.
        function timeCount() {  
        
            if(minutes !== -1){
                                
                if(seconds <= 60){

                    seconds = seconds - 1;
                    
                    // Setting out visual html of the current countdown time
                    document.getElementById("seconds").innerHTML = checkLeadingZero(minutes) + " : " + checkLeadingZero(seconds);  

                    // Using the setTimeout to wait 1 second before calling this function again.
                    pause = setTimeout(function() { 
                        timeCount(); 
                        }, 1000);
                    
                    if(seconds === 0){
                        // Clearing timeout and start all over when seconds reached 0
                        clearTimeout();
                        // Resseting seconds
                        seconds = 60;
                        
                        // Keeping an eye on the minutes that we are counting down from
                        // We only want to run when we havent reached 0
                        // BUG, but it doesnt really matter
                        if(minutes <= 9){
                            minutes = minutes - 1;
                        }
                    }
                }
            }
            else
            {
                // Just writing to the user that time is up.
                document.getElementById("seconds").innerHTML = "Tiden er gået !!";
                
                // Making sure our timeout is cleared
                clearTimeout();

                // Resseting our variables.
                minutes = 9;
                seconds = 60;
                timerStatus = 0;
                
                // Playing nice little sound when done.
                var sound = new Audio("Stop car sound effect.mp3"); // buffers automatically when created
                sound.play();
            }
        };

        function checkLeadingZero(number) {
            
            // If we dont have 2 numbers, we will insert leading 0.
            // Converting the number to string before checking length
            if(number.toString().length !== 2) {
                number = "0" + number;
            }

            return number;
        }

        // If the counter is not already running
        function startCount(){
            if(!timerStatus){
                timerStatus = 1;
                timeCount();
            }
        }
        
        // Pause the counter
        function pauseCount(){
                clearTimeout(pause);
                timerStatus = 0;
        }
        
        // Resset to counter
        function resetCount(){
                document.getElementById("seconds").innerHTML = "10 : 00";
                clearTimeout(pause);
                minutes = 9;
                seconds = 60;
                timerStatus = 0;
        }
</script>